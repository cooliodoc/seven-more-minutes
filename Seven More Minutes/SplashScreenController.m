//
//  SplashScreenController.m
//  Seven More Minutes
//
//  Created by George McKibbin on 3/08/12.
//  Copyright (c) 2012 George McKibbin. All rights reserved.
//

#import "SplashScreenController.h"
#import <Parse/Parse.h>
#import "GANTracker.h"

@interface SplashScreenController ()

@end

@implementation SplashScreenController
@synthesize appTableView;
@synthesize progressIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [Parse setApplicationId:@"dBSlO94IfqxUKZ8FNBolqRachD1UUpZ0uSbLAsIN"
                  clientKey:@"opQQ3UuVfKPvACHOqRYzLu7LhZvFCedLmWM0XUbe"];
    self.progressIndicator.hidden = NO;
    [self.progressIndicator startAnimating];
    PFQuery *query = [PFQuery queryWithClassName:@"appaweek"];
    [query orderByDescending:@"appNumber"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
     {
         if (!error)
         {
             NSMutableArray *filteredObjects = [objects mutableCopy];
             for (PFObject *object in objects)
             {
                 if ([[object valueForKey:@"appNumber"] integerValue] == 3)
                 {
                     [filteredObjects removeObjectAtIndex:[objects indexOfObject:object]];
                 }
             }
             self.myApps = [filteredObjects copy];
             [self.appTableView reloadData];
             [self.progressIndicator stopAnimating];
         }
     }];
    
    [[GANTracker sharedTracker] trackPageview:@"/splashScreen"
                                    withError:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [Parse setApplicationId:@"Hhw171S5DYrTZ1F70JyMRWjW5qQnWig9NQOQFroW"
                  clientKey:@"TCPbZxMkgA8DRPGQiq9D2olDfFMsh2QdvqKudQmz"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dark dirty.png"]];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.myApps count])
    {
        return [self.myApps count];
    }
    else
    {
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PFObject *app = [self.myApps objectAtIndex:indexPath.section];
    
    NSString *cellIdentifier = @"appaweek";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    UIImageView *appIcon = (UIImageView *)[cell viewWithTag:1];
    UILabel *appName = (UILabel *)[cell viewWithTag:2];
    UILabel *releaseDate = (UILabel *)[cell viewWithTag:3];
    UILabel *appDescription = (UILabel *)[cell viewWithTag:4];
    
    PFFile *icon = [app objectForKey:@"icon"];
    
    [appIcon setContentMode:UIViewContentModeScaleAspectFit];
    [appIcon setImage:[UIImage imageWithData:[icon getData]]];
    [appName setText:[NSString stringWithFormat:@"#%@ %@", [app objectForKey:@"appNumber"], [app objectForKey:@"appName"]]];
    [releaseDate setText:[app objectForKey:@"releaseDate"]];
    [appDescription setText:[app objectForKey:@"appDescription"]];

    
    
    return cell;
}

- (void)viewDidUnload
{
    [self setAppTableView:nil];
    [self setProgressIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PFObject *app = [self.myApps objectAtIndex:indexPath.section];
    NSString *itunesURL = [app objectForKey:@"itunesURL"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:itunesURL]];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[GANTracker sharedTracker] trackEvent:@"Clicked Another App"
                                         action:[app valueForKey:@"appName"]
                                          label:nil
                                          value:0
                                 withError:nil];
}

@end
