//
//  MainScreenViewController.m
//  Seven More Minutes
//
//  Created by George McKibbin on 4/08/12.
//  Copyright (c) 2012 George McKibbin. All rights reserved.
//

#import "MainScreenViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import <FBiOSSDK/FacebookSDK.h>
#import "GANTracker.h"

@interface MainScreenViewController ()
-(NSString *)twoDigitStringFromInt:(NSInteger)theInteger;
@end

@implementation MainScreenViewController

@synthesize challengeTimer;
@synthesize lifeGained;
@synthesize difficultyScrollView;
@synthesize socialLabel;
@synthesize mentalLabel;
@synthesize physicalLabel;
@synthesize emotionalLabel;
@synthesize socialButton;
@synthesize mentalButton;
@synthesize physicalButton;
@synthesize emotionalButton;
@synthesize socialFrame;
@synthesize mentalFrame;
@synthesize physicalFrame;
@synthesize emotionalFrame;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.difficultyScrollView setContentSize:CGSizeMake(600.0, 53.0
                                                         )];
    self.difficulty = @"warm";
    self.difficultyPage = 0;
    self.challengeNames = @[@"social", @"mental", @"physical", @"emotional"];
    self.difficultyNames = @[@"warm", @"simmering", @"scalding"];
    
    //Set up the BBCycling labels for challenges.
    self.challengeLabels = [NSArray arrayWithObjects:self.socialLabel,
                           self.mentalLabel,
                           self.physicalLabel,
                           self.emotionalLabel,
                           nil];
    
    for (BBCyclingLabel* label in self.challengeLabels)
    {
        label.font = [UIFont fontWithName:@"Heiti SC" size:18.0];
        
//        label.textColor = [UIColor colorWithRed:41.0/255.0
//                                          green:90.0/255.0
//                                           blue:209.0/255.0
//                                          alpha:1.0];
        label.textColor = [UIColor whiteColor];
        label.numberOfLines = 0;
        label.textAlignment = UITextAlignmentLeft;
        label.transitionDuration = 0.3;
        label.transitionEffect = BBCyclingLabelTransitionEffectScrollDown;
        label.clipsToBounds = YES;
        [label setText:@" " animated:NO];
    }
    

    [[self socialButton] setImage:[UIImage imageNamed:@"socialButtonDone.png"] forState:UIControlStateSelected];
    [[self mentalButton] setImage:[UIImage imageNamed:@"mentalButtonDone.png"] forState:UIControlStateSelected];
    [[self physicalButton] setImage:[UIImage imageNamed:@"physicalButtonDone.png"] forState:UIControlStateSelected];
    [[self emotionalButton] setImage:[UIImage imageNamed:@"emotionalButtonDone.png"] forState:UIControlStateSelected];
    
    self.buttons = [NSArray arrayWithObjects:socialButton, mentalButton, physicalButton, emotionalButton, nil];
    self.frames = @[ self.socialFrame, self.mentalFrame, self.physicalFrame, self.emotionalFrame ];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dark dirty.png"]];
}

- (void)viewDidUnload
{
    [self setChallengeTimer:nil];
    [self setLifeGained:nil];
    [self setDifficultyScrollView:nil];
    [self setSocialLabel:nil];
    [self setMentalLabel:nil];
    [self setPhysicalLabel:nil];
    [self setEmotionalLabel:nil];
    [self setSocialButton:nil];
    [self setMentalButton:nil];
    [self setPhysicalButton:nil];
    [self setEmotionalButton:nil];
    [self setSocialFrame:nil];
    [self setMentalFrame:nil];
    [self setPhysicalFrame:nil];
    [self setEmotionalFrame:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

// The tick for the timer, every 0.05 seconds
- (void)tick
{
    if (![self.challengeData valueForKey:@"endTime"] || ([[self.challengeData valueForKey:@"endTime"] compare:[NSDate date]] == NSOrderedAscending))
    {
        // Clear the challenge Data. Saving the current bonus life
        
        NSNumber *bonusLife = [self.challengeData objectForKey:@"bonusLife"];
        
        self.challengeData = [NSMutableDictionary dictionary];
        
        if (bonusLife)
        {
            [self.challengeData setValue:bonusLife forKey:@"bonusLife"];
        }
        
        // Set ChallengeEndDate to the end of the current hour
        NSDate *now = [NSDate date];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit fromDate:now];
        [components setHour:(components.hour + 1)];
        [self.challengeData setValue:[calendar dateFromComponents:components] forKey:@"endTime"];
        [self updateStores];
        
        // Fetch new challenges
        NSError* error;
        NSString* challengesURL = [[NSBundle mainBundle] pathForResource:@"challenges" ofType:@"csv"];
        NSString* challengesDoneURL = [[NSBundle mainBundle] pathForResource:@"challengesDone" ofType:@"csv"];
        NSString* challengesCSV = [NSString stringWithContentsOfFile:challengesURL encoding:NSUTF8StringEncoding error:&error];
        NSString* challengesDoneCSV = [NSString stringWithContentsOfFile:challengesDoneURL encoding:NSUTF8StringEncoding error:&error];
        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
            return;
        }
        
        NSArray* challengeCSVStrings = [challengesCSV componentsSeparatedByString:@"\n"];
        NSArray* challengeDoneCSVStrings = [challengesDoneCSV componentsSeparatedByString:@"\n"];
        int i = 0;
        for (NSString *challengeName in self.challengeNames)
        {
            for (NSString *difficultyName in self.difficultyNames)
            {
                NSArray *challenges = [[challengeCSVStrings objectAtIndex:i] componentsSeparatedByString:@"|"];
                NSArray *challengesDone = [[challengeDoneCSVStrings objectAtIndex:i] componentsSeparatedByString:@"|"];
                NSInteger randomInt = arc4random_uniform([challenges count] - 1) + 1;
                [self.challengeData setValue:[challenges objectAtIndex:randomInt] forKey:[NSString stringWithFormat:@"%@%@", challengeName, difficultyName]];
                [self.challengeData setValue:[challengesDone objectAtIndex:randomInt] forKey:[NSString stringWithFormat:@"%@%@Done", challengeName, difficultyName]];
                i++;
            }
        }
        
        [self updateStores];
    }
    
    // Set the challenge timer to be the remaining time in the hour
    self.challengeTimer.text = [self hourMinsSecsFromInt:[[self.challengeData valueForKey:@"endTime"] timeIntervalSinceDate:[NSDate date]]];
    
    // If the displayed life and actual life are different then adjust
    if ([self.challengeData objectForKey:@"bonusLife"] && [[self.challengeData objectForKey:@"bonusLife"] integerValue] != self.displayedBonus)
    {
        if ([[self.challengeData objectForKey:@"bonusLife"] integerValue] > self.displayedBonus)
        {
            if (((abs)([[self.challengeData objectForKey:@"bonusLife"] integerValue] - self.displayedBonus)) <= 3)
            {
                self.displayedBonus++;
            }
            else
            {
                self.displayedBonus += 3;
            }
        }
        else
        {
            if (((abs)([[self.challengeData objectForKey:@"bonusLife"] integerValue] - self.displayedBonus)) <= 3)
            {
                self.displayedBonus--;
            }
            else
            {
                self.displayedBonus -= 3;
            }
        }
        self.lifeGained.text = [self hourMinsSecsFromInt:self.displayedBonus];
        
        // If it has reached equity then vibrate;
        if ([[self.challengeData objectForKey:@"bonusLife"] integerValue] == self.displayedBonus)
        {
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        }
    }
    
    for (int j = 0; j < 4; j++)
    {
        NSString *keyToCheck;
        if ([self.challengeData objectForKey:[NSString stringWithFormat:@"%@Done", [self.challengeNames objectAtIndex:j]]])
        {
            keyToCheck = [self.challengeData objectForKey:[NSString stringWithFormat:@"%@Done", [self.challengeNames objectAtIndex:j]]];
            [[self.buttons objectAtIndex:j] setSelected:YES];
            [[self.frames objectAtIndex:j] setImage:[UIImage imageNamed:@"blackframeDone.png"]];
        }
        else
        {
            keyToCheck = [NSString stringWithFormat:@"%@%@", [self.challengeNames objectAtIndex:j], self.difficulty];
            [[self.buttons objectAtIndex:j] setSelected:NO];
            [[self.frames objectAtIndex:j] setImage:[UIImage imageNamed:@"blackframe.png"]];
        }
        if (![[[self.challengeLabels objectAtIndex:j] text] isEqualToString:[self.challengeData objectForKey:keyToCheck]])
        {
            BBCyclingLabel *label = [self.challengeLabels objectAtIndex:j];
            [label setText:[self.challengeData objectForKey:keyToCheck] animated:YES];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector: @selector(updateKVStoreItems:)
                                                 name: NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                                               object:[NSUbiquitousKeyValueStore defaultStore]];
    
    [[NSUbiquitousKeyValueStore defaultStore] synchronize];
    
    if (!self.challengeData)
    {
        if ([[NSUserDefaults standardUserDefaults] dictionaryForKey:@"challengeData"])
        {
            self.challengeData = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"challengeData"] mutableCopy];
        }
        else
        {
            self.challengeData = [NSMutableDictionary dictionary];
        }
    }
    
    self.lifeGained.text = [self hourMinsSecsFromInt:[[self.challengeData objectForKey:@"bonusLife"] integerValue]];
    self.displayedBonus = [[self.challengeData objectForKey:@"bonusLife"] integerValue];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.05
                                     target:self
                                   selector:@selector(tick)
                                   userInfo:nil
                                    repeats:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [[GANTracker sharedTracker] trackPageview:@"/mainScreen"
                                         withError:nil];
    //Check if this is the first time that the user has run the program and display the help if so
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([userDefaults valueForKey:@"sevenMinutesFirstRun"])
    {
        return;
    }
    else
    {
        // Display the help dialog
        [self help:self];
        [userDefaults setValue:@"YES" forKey:@"sevenMinutesFirstRun"];
        // 
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.timer invalidate];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)addBonusLife:(NSInteger)seconds
{
    if (![self.challengeData valueForKey:@"bonusLife"])
    {
        [self.challengeData setObject:[NSNumber numberWithInteger:seconds] forKey:@"bonusLife"];
        [self updateStores];
    }
    else
    {
        NSNumber *newLife = [NSNumber numberWithInteger:([[self.challengeData objectForKey:@"bonusLife"] integerValue] + seconds)];
        [self.challengeData setObject:newLife
                               forKey:@"bonusLife"];
        [self updateStores];
    }
    
    [[GANTracker sharedTracker] trackEvent:@"Add Bonus Life"
                                    action:nil
                                     label:nil
                                     value:seconds
                                 withError:nil];
}

- (void)takeBonusLife:(NSInteger)seconds
{
    NSNumber *newLife = [NSNumber numberWithInteger:([[self.challengeData objectForKey:@"bonusLife"] integerValue] - seconds)];
    [self.challengeData setObject:newLife
                           forKey:@"bonusLife"];
    [self updateStores];
    
    [[GANTracker sharedTracker] trackEvent:@"Take Bonus Life"
                                    action:nil
                                     label:nil
                                     value:seconds
                                 withError:nil];
}

- (NSInteger)numberOfButtonsPressed
{
    NSInteger count = 0;
    for (UIButton *button in self.buttons)
    {
        if (button.selected)
        {
            count++;
        }
    }
    return count;
}

- (IBAction)challengeDone:(UIButton *)sender
{
    sender.selected = !sender.selected;
    NSString *keyValue = [NSString stringWithFormat:@"%@%@Done", [self.challengeNames objectAtIndex:(sender.tag - 1)], self.difficulty];
    if (sender.selected)
    {
        [self.challengeData setValue:keyValue forKey:[NSString stringWithFormat:@"%@Done", [self.challengeNames objectAtIndex:(sender.tag - 1)]]];
        if ([self numberOfButtonsPressed] == 4)
        {
            [self addBonusLife:275];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations!" message:@"You successfully increased your social, mental, physical and emotional resistances and earned 7 minutes and 35 seconds of bonus life!\n\nIs it alright if we tell everyone on facebook how crazy amounts of awesome you are? (You'll get 60 seconds of bonus life for being extra social)" delegate:self cancelButtonTitle:@"Keep it a secret" otherButtonTitles:@"Tell the world!", nil];
            [alert show];
            
            NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
            [postParams setObject:@"http://itunes.apple.com/app/id550457674" forKey:@"link"];
            [postParams setObject:@"Seven More Minutes on iPhone" forKey:@"name"];
            [postParams setObject:@"Completing challenges earns you bonus (real) life." forKey:@"caption"];
            [postParams setObject:@"Challenge yourself by increasing your social, mental, physical and emotional resistances. Extend your life by up to 10 years" forKey:@"description"];
            
            NSString *message = [NSString stringWithFormat:@"*Fist Pump* I just extended my life by 7 minutes and 35 seconds because I:\n-%@\n-%@\n-%@\n-%@\n\n(%@ bonus life earned in total)",
                                 [self.challengeData objectForKey:[self.challengeData objectForKey:@"socialDone"]],
                                 [self.challengeData objectForKey:[self.challengeData objectForKey:@"mentalDone"]],
                                 [self.challengeData objectForKey:[self.challengeData objectForKey:@"physicalDone"]],
                                 [self.challengeData objectForKey:[self.challengeData objectForKey:@"emotionalDone"]],
                                 [self hourMinsSecsFromInt:[[self.challengeData valueForKey:@"bonusLife"] integerValue]]];
            [postParams setObject:message forKey:@"message"];
            
            self.postContent = [postParams copy];
        }
        else
        {
            [self addBonusLife:60];
        }
    }
    else
    {
        [self.challengeData removeObjectForKey:[NSString stringWithFormat:@"%@Done", [self.challengeNames objectAtIndex:(sender.tag - 1)]]];
        if ([self numberOfButtonsPressed] == 3)
        {
            [self takeBonusLife:275];
        }
        else
        {
            [self takeBonusLife:60];
        }
    }
    
}

- (IBAction)help:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Seven More Minutes Instructions" message:@"Seven More minutes is a game where you aim to complete one challenge from each of the four categories (social, mental, physical and emotional) every hour. Once you've completed a challenge press the button to lock it in.\n\nSwipe left and right down the bottom to change the difficulty at any time (warm, simmering, scalding).\n\nThe reward for completing a challenge is a minute of bonus life in real life. If you complete a challenge from all four categories then you get an extra 3 minutes and 35 seconds of bonus life.\n\nThis app is based on some research done by Jane McGonigal that found that if you challenge each of your four resistances once per hour then you get 7 minutes and 35 seconds of bonus life! If you do this every hour of your waking life you'll end up with a 10 YEARS of bonus life!" delegate:self cancelButtonTitle:@"Watch Jane's talk" otherButtonTitles: @"Thanks, I get it", nil];
    
    [alert show];
}

- (void)publishToFacebook
{
    if ([[FBSession activeSession] isOpen]
        && [[[FBSession activeSession] permissions] containsObject:@"publish_stream"])
    {
        
        
        [FBRequest startWithGraphPath:@"me/feed"
                           parameters:self.postContent
                           HTTPMethod:@"POST"
                    completionHandler:nil];
        
        if (![self.challengeData valueForKey:@"sentToFacebook"])
        {
            [self.challengeData setValue:@"YES" forKey:@"sentToFacebook"];
            [self addBonusLife:60];
            
            [[GANTracker sharedTracker] trackEvent:@"Posted To Facebook"
                                            action:nil
                                             label:nil
                                             value:0
                                         withError:nil];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([[alertView title] isEqualToString:@"Seven More Minutes Instructions"])
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.ted.com/talks/view/lang/en//id/1501"]];
        }
    }
    else if ([[alertView title] isEqualToString:@"Congratulations!"])
    {
        if (buttonIndex == 1)
        {
            if ([[FBSession activeSession] isOpen])
            {
                [self publishToFacebook];
                return;
            }
            [FBSession sessionOpenWithPermissions:@[@"publish_stream"]
                                completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                    if (!error) {
                                        [self publishToFacebook];
                                    }
                                }];
        }
    }
}

- (IBAction)goToSplash:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - iCloud Storage

- (void)updateKVStoreItems:(NSNotification*)notification {
    // Get the list of keys that changed.
    NSDictionary* userInfo = [notification userInfo];
    NSNumber* reasonForChange = [userInfo objectForKey:NSUbiquitousKeyValueStoreChangeReasonKey];
    NSInteger reason = -1;
    
    // If a reason could not be determined, do not update anything.
    if (!reasonForChange)
        return;
    
    // Update only for changes from the server.
    reason = [reasonForChange integerValue];
    if ((reason == NSUbiquitousKeyValueStoreServerChange) ||
        (reason == NSUbiquitousKeyValueStoreInitialSyncChange)) {
        // If something is changing externally, get the changes
        // and update the corresponding keys locally.
        NSArray* changedKeys = [userInfo objectForKey:NSUbiquitousKeyValueStoreChangedKeysKey];
        NSUbiquitousKeyValueStore* store = [NSUbiquitousKeyValueStore defaultStore];
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        
        // This loop assumes you are using the same key names in both
        // the user defaults database and the iCloud key-value store
        for (NSString* key in changedKeys) {
            id value = [store objectForKey:key];
            [userDefaults setObject:value forKey:key];
            
            if ([key isEqualToString:@"challengeData"])
            {
                self.challengeData = [value mutableCopy];
                self.displayedBonus = [[self.challengeData objectForKey:@"bonusLife"] integerValue];
                self.lifeGained.text = [self hourMinsSecsFromInt:self.displayedBonus];
            }
        }
    }
}

- (void)updateStores
{
    [[NSUbiquitousKeyValueStore defaultStore] setDictionary:self.challengeData forKey:@"challengeData"];
    [[NSUserDefaults standardUserDefaults] setObject:self.challengeData forKey:@"challengeData"];
}

#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    int page = (int)(targetContentOffset->x / 200);
    switch (page) {
        case 0:
            self.difficulty = @"warm";
            break;
        case 1:
            self.difficulty = @"simmering";
            break;
        case 2:
            self.difficulty = @"scalding";
        default:
            break;
    }
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if (self.difficultyPage != page) {
        switch (page) {
            case 0:
                self.difficulty = @"warm";
                break;
            case 1:
                self.difficulty = @"simmering";
                break;
            case 2:
                self.difficulty = @"scalding";
            default:
                break;
        }
        [[GANTracker sharedTracker] trackEvent:@"Change Difficulty"
                                        action:self.difficulty
                                         label:nil
                                         value:0
                                     withError:nil];
        self.difficultyPage = page;
    }
}

#pragma mark - Helpers

-(NSString *)twoDigitStringFromInt:(NSInteger)theInteger
{
    NSString *result = [NSString stringWithFormat:@"%u", theInteger];
    if ([result length] == 1) {
        result = [NSString stringWithFormat:@"0%@", result];
    }
    return result;
}

-(NSString *)hourMinsSecsFromInt:(NSInteger)theInteger
{
    NSInteger bonusSecs = theInteger;
    NSInteger bonusHours = (int)(bonusSecs / 3600);
    bonusSecs -= (bonusHours * 3600);
    NSInteger bonusMins = (int)(bonusSecs / 60);
    bonusSecs -= (bonusMins * 60);
    return [NSString stringWithFormat:@"%@:%@:%@", [self twoDigitStringFromInt:bonusHours], [self twoDigitStringFromInt:bonusMins], [self twoDigitStringFromInt:bonusSecs]];
}

@end
