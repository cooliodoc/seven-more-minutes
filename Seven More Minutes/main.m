//
//  main.m
//  Seven More Minutes
//
//  Created by George McKibbin on 31/07/12.
//  Copyright (c) 2012 George McKibbin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
