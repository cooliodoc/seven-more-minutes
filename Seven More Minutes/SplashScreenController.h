//
//  SplashScreenController.h
//  Seven More Minutes
//
//  Created by George McKibbin on 3/08/12.
//  Copyright (c) 2012 George McKibbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashScreenController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property NSArray *myApps;
@property (weak, nonatomic) IBOutlet UITableView *appTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;

@end
