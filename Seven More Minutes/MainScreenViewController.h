//
//  MainScreenViewController.h
//  Seven More Minutes
//
//  Created by George McKibbin on 4/08/12.
//  Copyright (c) 2012 George McKibbin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BBCyclingLabel.h"

@interface MainScreenViewController : UIViewController <UIScrollViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *challengeTimer;
@property (weak, nonatomic) IBOutlet UILabel *lifeGained;
@property (weak, nonatomic) IBOutlet UIScrollView *difficultyScrollView;
@property (weak, nonatomic) IBOutlet BBCyclingLabel *socialLabel;
@property (weak, nonatomic) IBOutlet BBCyclingLabel *mentalLabel;
@property (weak, nonatomic) IBOutlet BBCyclingLabel *physicalLabel;
@property (weak, nonatomic) IBOutlet BBCyclingLabel *emotionalLabel;
@property (weak, nonatomic) IBOutlet UIButton *socialButton;
@property (weak, nonatomic) IBOutlet UIButton *mentalButton;
@property (weak, nonatomic) IBOutlet UIButton *physicalButton;
@property (weak, nonatomic) IBOutlet UIButton *emotionalButton;
@property (weak, nonatomic) IBOutlet UIImageView *socialFrame;
@property (weak, nonatomic) IBOutlet UIImageView *mentalFrame;
@property (weak, nonatomic) IBOutlet UIImageView *physicalFrame;
@property (weak, nonatomic) IBOutlet UIImageView *emotionalFrame;

- (IBAction)challengeDone:(UIButton *)sender;
@property NSMutableDictionary *challengeData;
@property NSTimer *timer;
@property NSInteger displayedBonus;
@property NSDictionary *displayedChallenges;
@property NSString *difficulty;
@property NSString *displayedDifficulty;
@property NSInteger difficultyPage;
@property NSArray *challengeNames;
@property NSArray *difficultyNames;
@property NSArray *challengeLabels;
@property NSArray *buttons;
@property NSArray *frames;
@property NSDictionary *postContent;

- (IBAction)help:(id)sender;
- (IBAction)goToSplash:(id)sender;

@end
