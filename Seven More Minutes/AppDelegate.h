//
//  AppDelegate.h
//  Seven More Minutes
//
//  Created by George McKibbin on 31/07/12.
//  Copyright (c) 2012 George McKibbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSString* itunesURL;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
